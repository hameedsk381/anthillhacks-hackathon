Story for 3D Content of Devarayana Durga

About the project:
We are in the era where people make use of technology to collect information regarding  places before visiting the tourist spots. The 3D content of devarayana durga is built  where people can have a look and get a detailed information of that place inorder to know why they have to visit. The model that we develop helps the user to know about the history of that place and also experience the navigation through the 3d model.
This project will demonstrate such a model of Devarayanadurga in which, the interactive content will be built up using available and participatorily provided and curated content such that when visitors reach specific location in 3d space, depending on their extent of involvement chosen, it would open audio, video, web and simple immersive content for the visitors to experience. Moving away from the location would obviously halt the running of the content.  A 360°pan for each location along 3D content will make the experience near real, especially if historical /natural history content is brought out.  People will be encouraged to put in new content making this experience richer.

Project Workflow:

1] Initially collect all the dataset related to Devarayana durga. The dataset consists of: pano image, normal image, video, audio and text.

The (rough) demo video for Devarayana Durga prepared from the data available         can be found at the below link :
https://drive.google.com/open?id=18bivMwsA1frHB50kiFUo6kg5YNbh5PGJ
2] Start the gaming experience of DDhills by clicking the play button. A beautiful video of DDhill captured from the drown is displayed with the subtitles narrating the story of DDhills, the end of video contains the title “DEVARAYANA DURGA”.
3] The user is now switched to 3D terrain of DDhills where a  button arises on the screen saying “Start the Journey”. Once the button is clicked, the camera is made to traverse through 3D terrain using the effects of speeding up, slowing down   and also zooming in, zooming out of the camera.
4] After the traversing is complete, two button pops up slowly on the screen.

Journey (onClick step 5)
Explore  (onClick step 7)


5] When the user clicks on “Journey” he/she is redirected to a page where there are 4 options like:

1)ITB Bungalow 2)Oorubagilu 3)YogaNarasimhaSwamy Temple 4)Kalyani

The page is split into 4 sections and each section contains 3-4 images related to a particular monument and a button indicating the name of that place.We are in the era where people make use of technology to collect information regarding  places before visiting the tourist spots. The 3D content of devarayana durga is built  where people can have a look and get a detailed information of that place inorder to know why they have to visit. The model that we develop helps the user to know about the history of that place and also experience the navigation through the 3d model.

6] If the user clicks on a particular place then the details of that place appears of which the page contains image of the monument and text related to it, below we specify 4 options( or single button “begin” after clicking leads user to 4 options )  for the user inorder to get more details regarding the place in the form

audio - plays an audio related to that monument
2 )video -  plays an audio related to that monument
pano - displays the 360 view of that monument
3D - switches the user to a particular location on the terrain

7] When the “Explore” button is pressed the user is navigated to the 3D terrain of DDhills, there are markers(like glowing balls) at few locations of the terrain and the user can experience the walk through into the DDhills and explore the place, once the user reaches near the marker a text is displayed related to the monuments at that location , simultaneously an audio can be played.(OR can be redirected to step 6 where directly 4 options can be provided for the user to choose).
8] All the page contains the close button to navigate to the previous page.
