# Anthillhacks Hackathon


Anthillhacks Dec 2023

Being influenced by the dWebCamp event events, we have been looking forward to indulge in dWeb apps and explore how these may come in handy for our Anthillhacks use case and for our Internet independent COWMesh contexts. (ref Intro)

Now as we approach the next Anthillhacks event (Dec 25th 2023 to Jan 4, 2024), we have been talking to Techsoup.org who are sponsoring a few hackathon scholarships to kick off this move of the demo proceedings to an IPFS+Filecoin application.

For this we have identified 3 building block apps that can compose to help transform the "Anthillhacks Walkthrough" app into a dWeb app.

+ The heavy files such as the point-cloud of reconstructed buildings, the 360 panaroma images and the raster map can be ipfs/filecoin files. This in context should bring the heavy files closer to where the person is and there by increase the loading speed. Also, this should help make these files available offline (say in the COWMesh region) and help local community open these too.

+ The database that is feeding the Semantic Entities that are identifying the session contribution by participants need to be also an IPFS/IPNS object.

+ A browser-plugin that helps build the database during the Anthillhacks event by the participants. Typically, this plugin helps message the community through Web Annotation of a Wed document (including media such as audio, images and video) that is related to the event.

Putting these together as an upgrade would help bring about necessary conversations to go forward with the intention of inclusive tools that help present the activities at Anthillhacks and many other events.


See Wiki pages https://gitlab.com/servelots/dddweb/anthillhacks-hackathon/-/wikis/Archived-Anthillhacks-Web-Interface

Also these reference pages for ongoing work

https://anthillhacks.in

https://open.janastu.org/projects

https://gitlab.com/servelots/dddweb/anthillhacks-hackathon/-/blob/main/Archived_HTML_page.html

[Devarayanadurga_3dmodel_old](https://gitlab.com/servelots/dddweb/anthillhacks-hackathon/-/blob/4013d2532ec9a12e0c0ac5df38d7346bfdf3490b/devrayanadurga_early_model)



